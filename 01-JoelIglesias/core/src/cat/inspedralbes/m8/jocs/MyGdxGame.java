package cat.inspedralbes.m8.jocs;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

public class MyGdxGame extends ApplicationAdapter {
	SpriteBatch batch;
	Rectangle player;
	Texture texturePlayer, enemicTexture, disparTexture;
	final int TEMPS_ENTRE_APARICIO_ENEMIC_EN_SEGONS=1, VELOCITAT_NAU_JUGADOR = 300, VELOCITAT_NAU_ENEMIC = 200;
	List<Rectangle> enemics;
	List<Rectangle> disparsJugador;
	float tempsUltimEnemic=0;
	final float AMPLE_LASER = 20;
	final float ALT_LASER = 100;
	final float VELOCITAT_DISPAR_PLAYER = 300;
	
	
	@Override
	public void create () {//se ejecuta una vez al principio
		batch = new SpriteBatch();
		player= new Rectangle(0, 0, 100, 100);
		enemics = new ArrayList<Rectangle>();
		disparsJugador= new ArrayList<Rectangle>();
		
		texturePlayer = new Texture(Gdx.files.internal("naus/player.png"));
		enemicTexture = new Texture(Gdx.files.internal("naus/enemy.png"));
		disparTexture = new Texture(Gdx.files.internal("lasers/laserBlue.png"));
	}

	@Override
	public void render () {//Se ejecuta continuamente cuando quiere
		Gdx.gl.glClearColor(1, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		gestionarInput();
		
		actualitza();
		
		batch.begin();
		batch.draw(texturePlayer, player.x, player.y, player.width, player.height);
		for (Rectangle enemic : enemics) {
			batch.draw(enemicTexture, enemic.x, enemic.y, enemic.width, enemic.height);
		}
		
		for (Rectangle dispar : disparsJugador) {
			batch.draw(disparTexture, dispar.x, dispar.y, dispar.width, dispar.height);
		}
		batch.end();
	}
	
	@Override
	public void dispose () {//se ejecurta una vez al acabar
		batch.dispose();
		texturePlayer.dispose();
		enemicTexture.dispose();
	}
	
	
	private void actualitza() {
				
		//1. Si ha passat un cert temps apareix un enemic
		
		// Obtenim el temps que ha passat des de l'ultim dibuixat de pantalla. Es el temps d'un frame.
		float delta = Gdx.graphics.getDeltaTime();
	
		//L'acumulem al temps per controlar si ha d'apareixer un enemic
		tempsUltimEnemic += delta;
		if(tempsUltimEnemic > TEMPS_ENTRE_APARICIO_ENEMIC_EN_SEGONS) {
			int x=new Random().nextInt(300);
		//TODO: Genera una y aleatoria
			int y=800;

			Rectangle nouEnemic=new Rectangle(x,y,100,100);
			enemics.add(nouEnemic);
			
			tempsUltimEnemic=0; //Molt important resetejar el comptador
		}
		
		for (Rectangle dispar : disparsJugador) {
			//volem que els dispars vagin cap amunt. Per tant sumem a la y
			dispar.y += VELOCITAT_DISPAR_PLAYER * delta;
		}
		
		//2. Cada enemic baixa un poc. Es fa modificant la seva y
		for (Rectangle enemic : enemics) {
			enemic.y -= VELOCITAT_NAU_ENEMIC* delta;
		}
		
		for (Rectangle enemic : enemics) {
			if(enemic.overlaps(player)) {
			Gdx.app. exit();
			}
		}
		
		for (Iterator iterEnemics = enemics.iterator(); iterEnemics.hasNext();) {
			Rectangle enemic = (Rectangle) iterEnemics.next();

			for (Iterator iterDispar = disparsJugador.iterator(); iterDispar.hasNext();) {
				Rectangle dispar = (Rectangle) iterDispar.next();

				//Si l'enemic que estem mirant xoca amb el dispar que estem mirant volem eliminar-los als dos
				if(enemic.overlaps(dispar)) {
					iterDispar.remove();
					iterEnemics.remove();

					//Si no acabo el bucle de dispar, potser torna a accedir a un dispar ja inexistent
					break;
				}
			}
			
		}

		
		//Els enemics que surten de la pantalla per baix, son eliminats de la List
		//Eliminar d'una llista mentre s'esta recorrent no es pot fer amb un for normal.
		//Una manera es amb iterators
		for (Iterator iterator = enemics.iterator(); iterator.hasNext();) {
			Rectangle enemic = (Rectangle) iterator.next();
			
			if (enemic.y < -enemic.height) {
				iterator.remove();
			}			
		}
	}
	
	private void gestionarInput() {
		
		float delta = Gdx.graphics.getDeltaTime();
		
		if(Gdx.input.isKeyPressed(Keys.LEFT)) {
			player.x-=VELOCITAT_NAU_JUGADOR*delta;
		}
		if(Gdx.input.isKeyPressed(Keys.RIGHT)) {
			player.x+=VELOCITAT_NAU_JUGADOR*delta;
		}
		if (Gdx.input.isKeyPressed(Keys.UP)) {
			player.y += VELOCITAT_NAU_JUGADOR*delta;
		}
		if (Gdx.input.isKeyPressed(Keys.DOWN)) {
			player.y -= VELOCITAT_NAU_JUGADOR*delta;
		}
		
		int anchoPantalla=Gdx.graphics.getWidth(), alturaPantalla=Gdx.graphics.getHeight();
		
		if(player.x<0) {
			player.x=0;
		}
		if(player.x>(anchoPantalla-player.width)) {
			player.x=anchoPantalla-player.width;
		}
		if(player.y<0) {
			player.y=0;
		}
		if(player.y>(alturaPantalla-player.height)) {
			player.y=alturaPantalla-player.height;
		}
		
		if (Gdx.input.isKeyJustPressed(Keys.SPACE)) {
			//Fem que el dispar surti de la part central de dalt de la nau
			float x= player.x + player.width/2;
			float y = player.y + player.height; //Recordem que la y es el punt de baix a lesquerra. 
			disparsJugador.add(new Rectangle(x,y,AMPLE_LASER, ALT_LASER));
		}
	}

}
