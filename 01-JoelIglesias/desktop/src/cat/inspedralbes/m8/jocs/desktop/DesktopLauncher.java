package cat.inspedralbes.m8.jocs.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import cat.inspedralbes.m8.jocs.MyGdxGame;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
	    config.title = "Juego de naves";
	    config.width = 800;
	    config.height = 480;
		new LwjglApplication(new MyGdxGame(), config);
	}//asdf
}
